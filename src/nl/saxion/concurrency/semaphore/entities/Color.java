package nl.saxion.concurrency.semaphore.entities;

import java.util.Random;

/**
 * Created by marc on 10-12-14.
 */
public class Color{

	public static final String BLACK  = "Black";
	public static final String WHITE  = "White";
	public static final String YELLOW = "Yellow";
	public static final String BLUE   = "Blue";
	public static final String GREEN  = "Green";
	public static final String RED    = "Red";
	public static final String CYAN   = "Cyan";

	/**
	 * This method will return a new random color.
	 *
	 * @return The random color
	 */
	public static String randomColor() {
		int rand = new Random().nextInt(6);
		switch(rand){
			case 0:
				return WHITE;
			case 1:
				return YELLOW;
			case 2:
				return BLUE;
			case 3:
				return GREEN;
			case 4:
				return RED;
			case 5:
				return CYAN;
			default:
				return BLACK;
		}
	}

}