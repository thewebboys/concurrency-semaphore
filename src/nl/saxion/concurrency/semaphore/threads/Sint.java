package nl.saxion.concurrency.semaphore.threads;

import nl.saxion.concurrency.semaphore.Program;
import nl.saxion.concurrency.semaphore.threads.pieten.Piet;

/**
 * Created by marc on 3-12-14.
 */
public class Sint extends Thread {

	/** The program instance */
	private Program program;

	/**
	 * Constructor for the sint.
	 */
	public Sint(Program program){

		// Get program instance
		this.program = program;

	}

	/**
	 * Override toString for the object
	 * @return
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

	/**
	 * The run method for the sint thread.
	 */
	@Override
	public void run(){

		while( true ) {

			try {

				// Wait for a piet
				program.verzamel.acquire();

				// Probeer het verzameloverleg te regelen
				if( program.collectorWaiting.size() >= 4 && program.hasBlackPiet(program.collectorWaiting) ) {

					// Houd werkoverleg
					verzameloverleg();

					// Clear list
					int total = program.collectorWaiting.size();
					program.collectorWaiting.clear();

					// Let it go through
					program.pietVerzamelOverleg.release(total);

					// Keep it
					System.out.println(this + " | Verzameloverleg done");
					program.verzamelOverleg.release(total);

				// Probeer het werkoverleg te regelen
				} else if( program.workWaiting.size() >= Program.WACHTEN_WERKOVERLEG_PIETEN ) {

					// Houd werkoverleg
					werkoverleg();

					// Clear list
					int total = program.workWaiting.size();
					program.workWaiting.clear();

					// Let it go through
					program.pietWerkOverleg.release( total );

					// Keep it
					System.out.println(this + " | Werkoverleg done");
					program.werkOverleg.release( total );

				}


			} catch (InterruptedException e) {

				// Oops
				System.out.println(this + " | Oops, I crashed: " + e.getLocalizedMessage());

			}

		}
	}

	/**
	 * A 'verzameloverleg'.
	 */
	private void verzameloverleg() throws InterruptedException {
		program.verzameloverlegRunning = true;
		System.out.println(this + " | Verzameloverleg: " + program.collectorWaiting.size());
		for( Piet piet : program.collectorWaiting ) {
			System.out.println(this + " | VO | Piet: " + piet);
		}
		Thread.sleep((int) (Math.random() * 1000));
		program.verzameloverlegRunning = false;
	}

	/**
	 * A 'werkoverleg'.
	 */
	private void werkoverleg() throws InterruptedException {
		program.werkoverlegRunning = true;
		System.out.println(this + " | Werkoverleg: " + program.workWaiting.size());
		for( Piet piet : program.workWaiting ) {
			System.out.println(this + " | WO | Piet: " + piet);
		}
		Thread.sleep((int) (Math.random() * 1000));
		program.werkoverlegRunning = false;
	}

}
