package nl.saxion.concurrency.semaphore.threads.pieten;

import nl.saxion.concurrency.semaphore.Program;

/**
 * The collector piet thread.
 *
 * The collector piet will collect stuff.
 */
public class CollectorPiet extends Piet{

	/**
	 * Constructor for the collector piet.
	 *
	 * @param name
	 * @param color
	 */
	public CollectorPiet(String name, String color, Program program){
		super(name, color, program);
	}

	/**
	 * The run method for the collector piet thread.
	 */
	@Override
	public void run() {
		while( true ) {

			try {

				// Collect stuff
				collect();

				// Wait for a piet
				program.verzamel.release();

				// Probeer in verzameloverleg te komen.
				program.verzamelOverleg.acquire();

				// Got into it!
				System.out.println(this + " | Verzameloverleg acquired");
				program.collectorWaiting.add( this );

				// Wait for overleg
				program.pietVerzamelOverleg.acquire();
				System.out.println(this + " | PietVerzamelOverleg done");

			} catch (InterruptedException e) {

				// Oops
				System.out.println(this + " | Oops, I crashed: " + e.getLocalizedMessage());

			}

		}
	}

	/**
	 * Inside the method the collector piet will collect lists.
	 */
	private void collect() throws InterruptedException {
		System.out.println(this + " | Collect");
		sleep( (int)( Math.random() * 1000 ) );
	}

}
