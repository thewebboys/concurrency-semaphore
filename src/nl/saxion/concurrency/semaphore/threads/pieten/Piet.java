package nl.saxion.concurrency.semaphore.threads.pieten;

import nl.saxion.concurrency.semaphore.Program;

/**
 * Abstract class for a piet.
 */
public abstract class Piet extends Thread{

	// The name
	protected String name;

	// The color
	protected String color;

	// The program instance
	protected Program program;

	/**
	 * Constructor for a piet.
	 *
	 * @param name  The name of the piet
	 * @param color The color of the piet
	 */
	public Piet(String name, String color, Program program){

		// Set vars
		this.name = name;
		this.color = color;

		// Get instance
		this.program = program;
	}

	/**
	 * Get the color of the piet.
	 *
	 * @return
	 */
	public String getColor(){
		return color;
	}

	/**
	 * Override toString for piet.
	 *
	 * @return
	 */
	@Override
	public String toString(){
		return this.getClass().getSimpleName() + " (" + color + ")";
	}

}
