package nl.saxion.concurrency.semaphore.threads.pieten;

import nl.saxion.concurrency.semaphore.Program;
import nl.saxion.concurrency.semaphore.entities.Color;

/**
 * The work piet class.
 *
 * This piet will work and have some a ...
 */
public class WorkPiet extends Piet{

	/**
	 * Constructor for the workpiet
	 *
	 * @param name
	 * @param color
	 */
	public WorkPiet(String name, String color, Program program){
		super(name, color, program);
	}

	/**
	 * The run method for the work piet thread.
	 */
	@Override
	public void run() {
		while( true ){

			try {

				// Werk
				work();

				// Tell i am ready
				program.verzamel.release();

				// Mute
				program.mutex.acquire();

				// Are they already running?
				if( (this.color.equals( Color.BLACK ) && program.verzameloverlegRunning) || (!this.color.equals( Color.BLACK ) && program.werkoverlegRunning) ) {
					program.mutex.release();
				}

				// Spot in list?
				else if(this.color.equals( Color.BLACK ) && !program.hasBlackPiet( program.collectorWaiting ) && program.verzamelOverleg.tryAcquire() ) {

					// Got into it!
					System.out.println(this + " | Verzameloverleg acquired");
					program.collectorWaiting.add( this );

					// Clear mutex
					program.mutex.release();

					// Wait for overleg
					program.pietVerzamelOverleg.acquire();
					System.out.println(this + " | PietVerzamelOverleg done");

				}

				// Probeer in werkoverleg te komen.
				else if( program.werkOverleg.tryAcquire() ) {

					// Got into it!
					System.out.println(this + " | Werkoverleg acquired");
					program.workWaiting.add( this );

					// Clear mutex
					program.mutex.release();

					// Wait for overleg
					program.pietWerkOverleg.acquire();
					System.out.println(this + " | PietWerkOverleg done");

				} else program.mutex.release();

			} catch (Exception e) {

				// Oops
				System.out.println(this + " | Oops, I crashed: " + e.getLocalizedMessage());

			}

		}
	}

	/**
	 * Inside the method the work piet will work.
	 */
	private void work() throws InterruptedException {
		System.out.println(this + " | Werken");
		sleep( (int)( Math.random() * 1000 ) );
	}

}
