package nl.saxion.concurrency.semaphore;

import nl.saxion.concurrency.semaphore.threads.pieten.CollectorPiet;
import nl.saxion.concurrency.semaphore.threads.pieten.Piet;
import nl.saxion.concurrency.semaphore.threads.Sint;
import nl.saxion.concurrency.semaphore.threads.pieten.WorkPiet;
import nl.saxion.concurrency.semaphore.entities.Color;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * The main program class.
 *
 * This class is singleton and contains all important
 * variables. It will instantiate and start all instances,
 * including the threads.
 */
public class Program {

	/**
	 * The program starter.
	 *
	 * This method will start the program.
	 *
	 * @param args
	 */
	public static void main(String[] args){
		new Program().run();
	}

	//
	// Variables
	//

	/** The number of collector pieten */
	public static final int AANTAL_VERZAMEL_PIETEN = 6;

	/** The number of work pieten */
	public static final int AANTAL_WERK_PIETEN = 6;

	/** The size of werkoverleg */
	public static final int WACHTEN_WERKOVERLEG_PIETEN = 3;

	//
	// Private
	//

	/** De sint */
	private Sint sint;

	/** De werkpieten en verzamelpieten */
	private Piet[] workPieten, collectorPieten;

	//
	// Public
	//

	public boolean werkoverlegRunning = false, verzameloverlegRunning = false;

	/** Semaphore voor het verzamelen van de pieten */
	public Semaphore verzamel, mutex;

	/** Semaphore voor het werkoverleg */
	public Semaphore werkOverleg, pietWerkOverleg;

	/** Semaphore voor het verzameloverleg */
	public Semaphore verzamelOverleg, pietVerzamelOverleg;

	/** De lijsten voor het verzamel- en werkoverleg */
	public ArrayList<Piet> collectorWaiting, workWaiting;

	//
	// Construction
	//

	/**
	 * Constructor of the program.
	 * Will setup all instances
	 */
	public Program(){

		// Create lists
		workPieten = new Piet[AANTAL_WERK_PIETEN];
		collectorPieten = new Piet[AANTAL_VERZAMEL_PIETEN];
		workWaiting = new ArrayList<Piet>();
		collectorWaiting = new ArrayList<Piet>();

		//
		// Create semaphores
		//

		// Wake call for sint
		verzamel = new Semaphore(0, true);
		mutex = new Semaphore(1);

		// For 'werkoverleg'
		werkOverleg = new Semaphore( WACHTEN_WERKOVERLEG_PIETEN, true );
		pietWerkOverleg = new Semaphore(0, true);

		// For 'verzameloverleg'
		verzamelOverleg = new Semaphore( AANTAL_VERZAMEL_PIETEN + 1, true );
		pietVerzamelOverleg = new Semaphore( 0, true );

	}

	//
	// Methods
	//

	/**
	 * This method will run the program.
	 *
	 * First it wil create the sint and the pieten.
	 * After this all threads will be started.
	 */
	public void run() {

		//
		// Creation
		//

		// Create sint
		sint = new Sint(this);

		// Create collector pieten
		for(int i = 0; i < AANTAL_VERZAMEL_PIETEN; i++) {
			collectorPieten[ i ] = new CollectorPiet( "vp" + i, Color.randomColor(), this);
		}

		// Create work pieten
		for( int i = 0; i < AANTAL_WERK_PIETEN - 1; i++) {
			workPieten[ i ] = new WorkPiet( "wp" + i, Color.randomColor(), this);
		}
		// Make sure we have at least 1 black piet
		workPieten[AANTAL_WERK_PIETEN - 1] = new WorkPiet("wp" + (AANTAL_WERK_PIETEN - 1), Color.BLACK, this);

		//
		// Run instances
		//

		// Start collector pieten
		for( Piet piet : collectorPieten ) {
			piet.start();
		}

		// Start work pieten
		for( Piet piet : workPieten ) {
			piet.start();
		}

		// Start sint
		sint.start();

	}

	/**
	 * Is one of the pieten a black piet?
	 * @param pieten
	 * @return
	 */
	public boolean hasBlackPiet(ArrayList<Piet> pieten){
		for(int i = 0; i < pieten.size(); i++){
			if(pieten.get(i).getColor().equals(Color.BLACK)){
				return true;
			}
		}

		return false;
	}

}
