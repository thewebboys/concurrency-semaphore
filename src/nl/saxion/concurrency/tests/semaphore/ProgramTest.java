package nl.saxion.concurrency.tests.semaphore;

import nl.saxion.concurrency.semaphore.Program;
import nl.saxion.concurrency.semaphore.entities.Color;
import nl.saxion.concurrency.semaphore.threads.Sint;
import nl.saxion.concurrency.semaphore.threads.pieten.CollectorPiet;
import nl.saxion.concurrency.semaphore.threads.pieten.Piet;
import nl.saxion.concurrency.semaphore.threads.pieten.WorkPiet;
import org.junit.Test;

import java.util.ArrayList;

public class ProgramTest{

	@Test
	public void testHasBlackPiet(){
		Program program = new Program();

		ArrayList<Piet> pieten = new ArrayList<Piet>();

		assert !program.hasBlackPiet(pieten) : "hashBlackPiet should return false";

		pieten.add(new WorkPiet("wp1", Color.RED, program));
		assert !program.hasBlackPiet(pieten) : "hashBlackPiet should return false";

		pieten.add(new WorkPiet("wp2", Color.BLACK, program));
		assert program.hasBlackPiet(pieten) : "hashBlackPiet should return true";
	}

	/**
	 * Test the behaviour of the sint
	 * @throws InterruptedException
	 */
	@Test
	public void testSint(){
		Program program = new Program();
		Sint sint = new Sint(program);
		
		sint.start();

		try{
			Thread.sleep(1000);
		} catch(InterruptedException e){
			e.printStackTrace();
		}

		assert sint.getState().toString() == "WAITING" : "Sint Should be in waiting state";
	}

	/**
	 * Test the behaviour of 2 pieten
	 */
	@Test
	public void testPieten(){
		Program program = new Program();
		ArrayList<Piet> pieten = new ArrayList<Piet>();
		pieten.add(new WorkPiet("wp1", Color.RED, program));
		pieten.add(new WorkPiet("wp2", Color.BLACK, program));

		for(Piet piet : pieten){
			piet.start();
			String state = "";
			try{
				Thread.sleep(1000);
				state = piet.getState().toString();
			} catch(InterruptedException e){
				e.printStackTrace();
			}

			assert state == "WAITING" : "Piet state should be Waiting";
		}
	}

	/**
	 * Test asserts for Work meetings
	 */
	@Test
	public void testWerkOverleg(){
		Program program = new Program();

		int total = Program.WACHTEN_WERKOVERLEG_PIETEN;

		assert program.werkOverleg.availablePermits() == total : "Should be " + total;
		Piet piet = new WorkPiet("wp1", Color.RED, program);
		piet.start();

		try{
			Thread.sleep(1000);
		} catch(InterruptedException e){
			e.printStackTrace();
		}

		assert program.werkOverleg.availablePermits() == total -1 : "Should be " + (total - 1);
		assert program.mutex.availablePermits() == 1 : "Mutex availablePermits should be 1";

	}
	/**
	 * Test asserts for Collector meetings
	 */
	@Test
	public void testVerzamelOverleg(){
		int total = Program.AANTAL_VERZAMEL_PIETEN + 1;

		Program program = new Program();

		ArrayList<Piet> pieten = new ArrayList<Piet>();
		pieten.add(new CollectorPiet("cp1", Color.RED, program));
		pieten.add(new CollectorPiet("cp2", Color.BLUE, program));
		pieten.add(new CollectorPiet("cp3", Color.YELLOW, program));
		pieten.add(new WorkPiet("wp1", Color.BLACK, program));

		assert program.verzamelOverleg.availablePermits() == total : "Should be " + total;

		for(Piet piet : pieten){
			piet.start();
		}

		try{
			Thread.sleep(1000);
		} catch(InterruptedException e){
			e.printStackTrace();
		}

		assert program.verzamelOverleg.availablePermits() == total - 4 : "Should be " + (total - 4);
		assert program.mutex.availablePermits() == 1 : "Mutex availablePermits should be 1";
		assert program.collectorWaiting.size() >= 4: "Threir should be at least 4 pieten in waiting";
		assert program.hasBlackPiet(pieten) : "Their should be a black piet";
	}

}